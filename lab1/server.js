var net = require('net');
var fs = require('fs');
var clients = [];
var server = net.createServer(function(connection) {
    console.log('client %s:%s connected', connection.remoteAddress, connection.remotePort);
    clients.push(connection.remoteAddress);
    var err_command = 'Invalid command, please try again';
    connection.on("data", data => {
        console.log("Server got: " + data);
        if(String(data) === "getJson"){

            fs.readFile('events.json', 'utf8', function (err,data) {
                if (err) {
                    return console.log(err);
                }
                console.log("Server sent: " + data);
                connection.write(data.toString() + '\r\n');
            });

        }
        else if(String(data) === "numOfObj"){

            fs.readFile('events.json', 'utf8', function (err,data) {
                if (err) { return console.log(err);}
                var eventsArray = JSON.parse(data);
                console.log(eventsArray.length);
                connection.write(eventsArray.length.toString() + '\r\n')

            });
        }
        else if(String(data) === "clients"){
            console.log(clients.toString());
            connection.write(clients.toString() + '\r\n')
        }
        else if(data.toString().indexOf("name") > -1 || data.toString().indexOf("place") > -1 || data.toString().indexOf("price") > -1 || data.toString().indexOf("audience") > -1 || data.toString().indexOf("date") > -1){

            sortBy = data.toString();
            fs.readFile('events.json', 'utf8', function (err,data) {
                if (err) {
                    return console.log(err);
                }
                var eventsArray = JSON.parse(data);

                if(sortBy === "price" || sortBy === "audience"){eventsArray.sort((a, b) => a[sortBy] - b[sortBy])}
                else{eventsArray.sort(function(a, b){return a[sortBy].localeCompare(b[sortBy]);})}
                console.log(eventsArray);
                connection.write(JSON.stringify(eventsArray) + '\r\n');
            });
        }
        else {
            console.log("Server sent: " + err_command);
            connection.write(err_command + '\r\n');  // send message to client
        }
    });

    connection.on('end', function() {
        console.log('client disconnected');
    });


    connection.pipe(connection);
});

server.listen(8080, function() {
    console.log('server is listening');
});