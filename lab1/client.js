const net = require('net');
const stdin = process.openStdin();
const connectionOptions = {
    host: "localhost",
    port: 8080
};

const client = net.connect(connectionOptions, () => {
    console.log('Connected to server!\nEnter your command(getJson, break, numOfObj, clients, or fieldname to get sorted array: name/owner/age/owner age/birthdate *param(without *)* ):\n');
});
// receive data from server
client.on('data', (data) => {
    console.log("Server: " + data.toString());
    console.log("Enter your command(getJson, break, numOfObj, clients, or fieldname to get sorted array: name/place/price/audience/date ):\n");
});
// when client disconnected
client.on('end', () => {
    console.log('disconnected from server');
    stdin.removeListener('data', stdinDataListener);  // unsubscribe
    stdin.destroy();  // close stdin
});
const stdinDataListener = function(data) {

    let str = data.toString().trim();
    console.log("You entered: " + str);
    if (str === "break") {
        client.end();  // disconnect client from server
    } else {
        client.write(str);  // send input string to server
    }
};
stdin.addListener('data', stdinDataListener);