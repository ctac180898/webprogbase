var express = require('express');

const crypto = require('crypto');
const session = require('express-session');
const passport = require('passport');
var router = express.Router();
var Event = require('../dbase.js').Event;
var User = require('../dbase.js').User;
var fs = require('fs');
var NodeGeocoder = require('node-geocoder');

var formatter1 = {
    format: function(data) {
        var strings = [];
        strings.push(data[0].longitude);
        strings.push(data[0].latitude);
        console.log("strings");
        return strings;
    },
}

var options = {
    provider: 'google',
    httpAdapter: 'https', // Default
    apiKey: 'AIzaSyBMmBOKthuQIP6qy9yNqffb-9p6nVvvCl0', // for Mapquest, OpenCage, Google Premier
    formatter: formatter1
};

var geocoder = NodeGeocoder(options);
const sessionSecret = '$EQRET@№GENT';
const serverSalt = "do!hURTS@lT";

function hash(pass, em) {
    return crypto.createHash('md5').update(em + pass + serverSalt).digest("hex");
}

router.get('/', function(req, res) {
    Event.find(function (err, events) {
        console.log(events.length);
        if (err) console.error(err.stack || err);
        if (events.length < 6) {
            //console.log(events);
            res.render('main', {events: events, pages: 1, message: "no"});
        }
        else {
            //console.log(events);
            var pages = (events.length/5) + 1;
            pages = pages - pages%1;
            console.log(pages);
            if(req.query.page)
                var page = req.query.page;
            else
                var page = 1;
            console.log(page);
            var startPos = page * 5 - 5;
            var lastPos = startPos + 5;
            var evarr = events.slice(startPos, lastPos);
            console.log(evarr);
            res.render('main', {events: evarr, pages: pages, message: "no"});
        }
    });
});

router.get('/event', function(req, res) {
    Event.findOne({_id: req.query.id}, function (err, event) {
        console.log(event);
        res.render('event', {event: event});
    });
});

router.get('/delete', function(req, res){
    console.log(req.query.id);
    Event.remove({_id: req.query.id}, function(err){
        res.redirect('/');
    })
})

router.post('/sorted', function(req, res){
    console.log(req.body.fname);
    Event.find({name: req.body.fname}, function(err, event){

        if(event.length > 0)
            res.render('event', {event: event[0]});
        else{
            Event.find(function (err, events) {
                console.log(events.length);
                if (err) console.error(err.stack || err);
                if (events.length < 6) {
                    res.render('main', {events: events, pages: 1, message: "no such event found"});
                }
                else {
                    var pages = (events.length/5) + 1;
                    pages = pages - pages%1;
                    console.log(pages);
                    if(req.query.page)
                        var page = req.query.page;
                    else
                        var page = 1;
                    console.log(page);
                    var startPos = page * 5 - 5;
                    var lastPos = startPos + 5;
                    var evarr = events.slice(startPos, lastPos);
                    console.log(evarr);
                    res.render('main', {events: evarr, pages: pages, message: "no such event found"});
                }
            });
        }
    })
})

router.post('/addevent', function(req, res){

    var geo = {};
    geocoder.geocode(req.body.place, function(err, res) {
        console.log("res: " + res.toString());
        var res1 = res.toString().split(',');
        geo.lng = res1[0];
        geo.lat = res1[1];
        console.log("geo: " + res1);
        var evsIm = req.files.evImage;
        var base64String = evsIm.data.toString('base64');
        var event = new Event({
            name: req.body.name,
            city: req.body.city,
            place: req.body.place,
            link: req.body.link,
            price: req.body.price,
            date: req.body.date,
            loc: [geo.lng, geo.lat],
            image: base64String,
            moderated: false
        });
        console.log(event);

        event.save(function(err, event) {
            if (err) return console.error(err);
            console.dir("Success");
        });
    });
    res.redirect('/');
})

module.exports = router;
