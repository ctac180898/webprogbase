var express = require('express');
var router = express.Router();
var fs = require('fs');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get('/event', function(req, res, next) {
  var id = req.query.id;
  fs.readFile('../events.json', 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    console.log("Server sent: " + data);
    var events = JSON.parse(data);
    var event = events[id];
    res.render('event', {event: event});
  });
});

module.exports = router;
