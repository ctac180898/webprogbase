var express = require('express');
var router = express.Router();
var fs = require('fs');
/* GET users listing. */

router.get('/', function(req, res, next) {
    console.log("rout");
    fs.readFile('../events.json', 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        console.log("Server sent: " + data);
        var events = JSON.parse(data);
        res.render('events', {events: events});
    });
});

module.exports = router;
