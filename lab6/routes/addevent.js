var express = require('express');
var router = express.Router();
var Event    = require('../dbase.js').Event;
var NodeGeocoder = require('node-geocoder');

var options = {
    provider: 'google',

    // Optional depending on the providers
    httpAdapter: 'https', // Default
    apiKey: 'AIzaSyAz46THD37Ca33epljAk9j3399ISdgbKOU', // for Mapquest, OpenCage, Google Premier
    formatter: 'string', // 'gpx', 'string', ...
    formatterPattern: '%lo %la',
};

var geocoder = NodeGeocoder(options);

router.get('/', function(req, res) {
    res.render('addevent');
});

router.post('/', function(req, res) {

    console.log("Pooooost");
    var geo = {};
    geocoder.geocode(req.body.place, function(err, res) {
        console.log("geo before: " + res);
        var res1 = res.toString().split(' ');
        console.log("geo after: lon " + res1[0] + " lat " + res1[1]);
        geo.lng = res1[0];
        geo.lat = res1[1];

        var eevent = new Event({
            name: req.body.name,
            city: req.body.city,
            place: req.body.place,
            price: req.body.price,
            date: req.body.date,
            lng: res1[0],
            lat: res1[1],
        });

        eevent.save(function(err, eevent) {
            if (err) return console.error(err);
            console.dir("Success");
        });
    });


    res.redirect('/');
});

module.exports = router;